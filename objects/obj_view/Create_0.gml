/// @description set camera
camera = camera_create();

viewxmax = 640;
viewymax = 352;
viewxmin = 0;
viewymin = 0;
xto = x;
yto = y;

var vm = matrix_build_lookat(x,y,-10,x,y,0,0,1,0);
var pm = matrix_build_projection_ortho(640,352,1,10000);

camera_set_view_mat(camera,vm);
camera_set_proj_mat(camera,pm);

view_camera[0] = camera;
