/// @description move view

if (obj_player.y+(obj_player.sprite_height/2) > viewymax)
{
	viewymax += 352;
	viewymin += 352;
	yto+= 352;
}
if (obj_player.x+(obj_player.sprite_width/2) > viewxmax)
{
	viewxmax += 640;
	viewxmin += 640;
	xto+= 640;	
}
if (obj_player.y+(obj_player.sprite_height/2) < viewymin)
{
	viewymax -= 352;
	viewymin -= 352;
	yto-= 352;
}
if (obj_player.x+(obj_player.sprite_width/2) < viewxmin)
{
	viewxmax -= 640;
	viewxmin -= 640;
	xto-= 640;	
}

x += (xto - x)/15;
y += (yto - y)/15;

var vm = matrix_build_lookat(x,y,-10,x,y,0,0,1,0);
camera_set_view_mat(camera,vm);
