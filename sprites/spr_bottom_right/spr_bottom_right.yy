{
    "id": "b0c03358-6b5f-4dae-af14-d530d22ab7f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bottom_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e698034-6686-41bb-8949-dcb5e7d7985c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0c03358-6b5f-4dae-af14-d530d22ab7f4",
            "compositeImage": {
                "id": "d2c8656a-b916-4042-94b7-816a6a773266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e698034-6686-41bb-8949-dcb5e7d7985c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a918588b-2cbd-41e1-b32e-7d78b09f9859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e698034-6686-41bb-8949-dcb5e7d7985c",
                    "LayerId": "efd6e62c-b724-4083-9729-dd6b98587ec0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "efd6e62c-b724-4083-9729-dd6b98587ec0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0c03358-6b5f-4dae-af14-d530d22ab7f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}