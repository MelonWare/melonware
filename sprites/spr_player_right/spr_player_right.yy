{
    "id": "c1ed882a-d70f-40a9-857d-ee8116857eb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d63a8dd-d03c-4908-ac87-e827f64710a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1ed882a-d70f-40a9-857d-ee8116857eb8",
            "compositeImage": {
                "id": "8b5f2ab7-6190-4fed-85a9-1760e3a5f67e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d63a8dd-d03c-4908-ac87-e827f64710a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a2b560a-10c2-4c31-9230-2f93c021d29b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d63a8dd-d03c-4908-ac87-e827f64710a3",
                    "LayerId": "28639195-2dfc-469d-8f4b-60f48d4b3bf4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "28639195-2dfc-469d-8f4b-60f48d4b3bf4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1ed882a-d70f-40a9-857d-ee8116857eb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}