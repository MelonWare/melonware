{
    "id": "e38bb5a0-6e67-4087-8ebc-20b238e91c51",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bottom_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ef759b1-2941-4756-85ef-4f3c0142081b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e38bb5a0-6e67-4087-8ebc-20b238e91c51",
            "compositeImage": {
                "id": "8f06f562-c8c2-4e89-8f88-5ec7e40ea410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ef759b1-2941-4756-85ef-4f3c0142081b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b1e3fcd-4aed-42fa-b2b6-dcf40f23413e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ef759b1-2941-4756-85ef-4f3c0142081b",
                    "LayerId": "e01621c4-94d6-4db7-a439-626c00dd4073"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e01621c4-94d6-4db7-a439-626c00dd4073",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e38bb5a0-6e67-4087-8ebc-20b238e91c51",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}