{
    "id": "56504dc7-93c8-45ae-837b-ad57b6fa2717",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b704cc38-357f-4acf-8f1f-82f75ac5bc88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56504dc7-93c8-45ae-837b-ad57b6fa2717",
            "compositeImage": {
                "id": "69d410ed-e0ab-43ed-bbc7-dd096f012cb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b704cc38-357f-4acf-8f1f-82f75ac5bc88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1b960a7-2254-4f5e-84c0-bc1d8b7fd76a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b704cc38-357f-4acf-8f1f-82f75ac5bc88",
                    "LayerId": "1e2acbe9-3d81-4d66-a52f-49879d0d4200"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1e2acbe9-3d81-4d66-a52f-49879d0d4200",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56504dc7-93c8-45ae-837b-ad57b6fa2717",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}