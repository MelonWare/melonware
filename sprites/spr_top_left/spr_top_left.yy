{
    "id": "b1b88b29-76df-4f4d-8861-d43dd5ea5132",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_top_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5887d50f-f9c1-4d2d-ae9d-e1a5183d6287",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b88b29-76df-4f4d-8861-d43dd5ea5132",
            "compositeImage": {
                "id": "ccecbc12-76cd-4bf1-bd2e-90d273553b8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5887d50f-f9c1-4d2d-ae9d-e1a5183d6287",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d9c324b-e57c-46fe-9b69-bdf0e0ca0140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5887d50f-f9c1-4d2d-ae9d-e1a5183d6287",
                    "LayerId": "228c9bf9-4693-485f-98c4-35faff864487"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "228c9bf9-4693-485f-98c4-35faff864487",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1b88b29-76df-4f4d-8861-d43dd5ea5132",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}