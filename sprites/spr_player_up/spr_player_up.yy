{
    "id": "a8a60b44-b291-49ed-8e33-44638228dd9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3e0a53e-b022-40ff-aeac-432512a3952b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8a60b44-b291-49ed-8e33-44638228dd9c",
            "compositeImage": {
                "id": "870dbf25-710f-4c68-9550-b6eea2559b7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3e0a53e-b022-40ff-aeac-432512a3952b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a89877d-d647-4127-a942-c05f2905f47a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3e0a53e-b022-40ff-aeac-432512a3952b",
                    "LayerId": "1f34a2b3-c3ad-476e-a444-1586aa3081bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1f34a2b3-c3ad-476e-a444-1586aa3081bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8a60b44-b291-49ed-8e33-44638228dd9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}