{
    "id": "6747c9e2-49d8-415a-9dcb-48dd8c31ef7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a01bc3ca-3e6f-422d-9fbf-63ab17d9ad77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6747c9e2-49d8-415a-9dcb-48dd8c31ef7d",
            "compositeImage": {
                "id": "5fa57086-a695-44a0-a7cb-c2099f50113d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a01bc3ca-3e6f-422d-9fbf-63ab17d9ad77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59304216-eb8d-4b5b-b661-a09277c60135",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a01bc3ca-3e6f-422d-9fbf-63ab17d9ad77",
                    "LayerId": "2fb10701-783f-45da-9d20-3a89fc6463dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2fb10701-783f-45da-9d20-3a89fc6463dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6747c9e2-49d8-415a-9dcb-48dd8c31ef7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}