{
    "id": "63dc193c-6d98-4b74-8873-69a4df90841a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f5d54d1-3dc6-4dd3-ad28-be3f37d272fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63dc193c-6d98-4b74-8873-69a4df90841a",
            "compositeImage": {
                "id": "34517005-4491-4831-aea4-9acffe3682f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f5d54d1-3dc6-4dd3-ad28-be3f37d272fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6953f8f9-51d6-4dac-ae09-404b3372417a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f5d54d1-3dc6-4dd3-ad28-be3f37d272fb",
                    "LayerId": "7a6d4578-3276-4c74-b208-d10a18e110f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7a6d4578-3276-4c74-b208-d10a18e110f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63dc193c-6d98-4b74-8873-69a4df90841a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}